const express = require('express');
const ideasRouter = express.Router();

const { getAllFromDatabase,
  getFromDatabaseById,
  addToDatabase,
  updateInstanceInDatabase,
  deleteFromDatabasebyId } = require('./db.js');

const checkMillionDollarIdea = require('./checkMillionDollarIdea.js');

const { validateProps } = require('./helpers.js');

const ideasString = 'ideas';

ideasRouter.param('ideaId', (req, res, next, id) => {
  const foundIdea = getFromDatabaseById(ideasString, id);
  if (foundIdea) {
    req.idea = foundIdea;
    next();
  } else {
    res.status(404).send('Idea Not Found');
  }
});

ideasRouter.get('/', (req, res, next) => {
  const ideas = getAllFromDatabase(ideasString);
  res.send(ideas);
});

const requiredIdeaProps = [
  'name',
  'description',
  'weeklyRevenue',
  'numWeeks'
]

ideasRouter.post('/', checkMillionDollarIdea, (req, res, next) => {
  const newIdea = req.body;
  if (validateProps(newIdea, requiredIdeaProps)) {
    addToDatabase(ideasString, newIdea);
    res.status(201).send(newIdea);
  } else {
    res.status(400).send(`Idea Was Not Added`);
  }
});

ideasRouter.get('/:ideaId', (req, res, next) => {
  res.send(req.idea);
})

ideasRouter.put('/:ideaId', checkMillionDollarIdea, (req, res, next) => {
  let updatedIdea = req.newIdea;
  if (updatedIdea.id !== req.idea.id) {
    res.status(400).send('Cannot update idea ID');
  } else {
    updatedIdea = updateInstanceInDatabase(ideasString, updatedIdea);
    res.status(200).send(updatedIdea);
  }
})

ideasRouter.delete('/:ideaId', (req, res, next) => {
  deleteFromDatabasebyId(ideasString, req.idea.id);
  res.status(204).send();
});

module.exports = ideasRouter;

