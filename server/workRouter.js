const express = require('express');
const workRouter = express.Router({mergeParams: true});

const { addToDatabase,
  createWork,
  getAllFromDatabase,
  getFromDatabaseById,
  updateInstanceInDatabase,
  deleteFromDatabasebyId } = require('./db.js');

const { validateProps, getMinionFromWork } = require('./helpers.js');

const workString = 'work';

workRouter.param('workId', (req, res, next, id) => {
  const foundWork = getFromDatabaseById(workString, id);
  if (foundWork) {
    req.work = foundWork;
    next();
  } else {
    res.status(404).send('Work Not Found');
  }
});


workRouter.get('/', (req, res, next) => {
  const work = getAllFromDatabase(workString);
  const minionWork = getMinionFromWork(work, req.minion);
  res.send(minionWork);
});

const requiredWorkProps = [
  'description',
  'hours',
  'minionId',
  'title'
]

workRouter.post('/', (req, res, next) => {
  const newWork = req.body;
  if (validateProps(newWork, requiredWorkProps)) {
    addToDatabase(workString, newWork);
    res.status(201).send(newWork);
  } else {
    res.status(404).send(`Work Not Added`);
  }
});

workRouter.put('/:workId', (req, res, next) => {
  let updatedWork = req.body;
  if (updatedWork.id !== req.work.id) {
    res.status(400).send('Cannot update work ID');
  } else {
    updatedWork = updateInstanceInDatabase(workString, updatedWork);
    res.status(200).send(updatedWork);
  }
})


workRouter.delete('/:workId', (req, res, next) => {
  deleteFromDatabasebyId(workString, req.work.id);
  res.status(204).send();
});

module.exports = workRouter;

