const validateProps = (obj, props) => {
  return props.every( prop => {
    return obj.hasOwnProperty(prop);
  })
}

const getMinionFromWork = (work, minion) => {
  return work.filter((singleWork) => {
    return singleWork.minionId === minion.id;
  })
}

module.exports = {
  validateProps,
  getMinionFromWork
};

