const express = require('express');
const meetingsRouter = express.Router();

const { getAllFromDatabase,
  addToDatabase,
  createMeeting,
  deleteAllFromDatabase } = require('./db.js');

const { validateProps } = require('./helpers.js');

const meetingsString = 'meetings';

meetingsRouter.get('/', (req, res, next) => {
  const meetings = getAllFromDatabase(meetingsString);
  res.send(meetings);
});

const requiredMeetingProps = [
  'time',
  'date',
  'day',
  'note'
];

meetingsRouter.post('/', (req, res, next) => {
  const newMeeting = createMeeting();
  if (validateProps(newMeeting, requiredMeetingProps)) {
    addToDatabase(meetingsString, newMeeting);
    res.status(201).send(newMeeting);
  } else {
    res.status(400).send(`Meeting Was Not Added`);
  }
});

meetingsRouter.delete('/', (req, res, next) => {
  deleteAllFromDatabase(meetingsString);
  res.status(204).send();
});

module.exports = meetingsRouter;

