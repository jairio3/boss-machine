const express = require('express');
const apiRouter = express.Router();

const ideasRouter = require('./ideasRouter.js');
const meetingsRouter = require('./meetingsRouter.js');
const minionsRouter = require('./minionsRouter.js');
const workRouter = require('./workRouter.js');

apiRouter.use('/ideas', ideasRouter);
apiRouter.use('/meetings', meetingsRouter);
apiRouter.use('/minions', minionsRouter);
minionsRouter.use('/:minionId/work', workRouter);

module.exports = apiRouter;
