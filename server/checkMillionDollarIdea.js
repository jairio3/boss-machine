const checkMillionDollarIdea = (req, res, next) => {

  const newIdea = req.body;

  if (newIdea.numWeeks && newIdea.weeklyRevenue) {

    const totalValue = newIdea.numWeeks * newIdea.weeklyRevenue;

    if (totalValue >= 1000000) {
      req.newIdea = req.body;
      next()
    } else {
      res.status(400).send(`Idea does not have a total value over one million dollars.`)
    }

  } else {
    res.status(400).send(`Idea does not have numWeeks or weeklyRevenue properties.`)
  }

};

// Leave this exports assignment so that the function can be used elsewhere
module.exports = checkMillionDollarIdea;
