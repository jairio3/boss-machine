const express = require('express');
const minionsRouter = express.Router();

const { getAllFromDatabase,
  getFromDatabaseById,
  addToDatabase,
  updateInstanceInDatabase,
  deleteFromDatabasebyId } = require('./db.js');

const { validateProps } = require('./helpers.js');

const minionsString = 'minions';

minionsRouter.param('minionId', (req, res, next, id) => {
  const foundMinion = getFromDatabaseById(minionsString, id);
  if (foundMinion) {
    req.minion = foundMinion;
    next();
  } else {
    res.status(404).send('Minion Not Found');
  }
});

minionsRouter.get('/', (req, res, next) => {
  const minions = getAllFromDatabase(minionsString);
  res.send(minions);
});

const requiredMinionProps = [
  'name',
  'title',
  'salary',
  'weaknesses'
];

minionsRouter.post('/', (req, res, next) => {
  const newMinion = req.body;
  if (validateProps(newMinion, requiredMinionProps)) {
    addToDatabase(minionsString, newMinion);
    res.status(201).send(newMinion);
  } else {
    res.status(400).send(`Minion Was Not Added`);
  }
});

minionsRouter.get('/:minionId', (req, res, next) => {
  res.send(req.minion);
})

minionsRouter.put('/:minionId', (req, res, next) => {
  let updatedMinion = req.body;
  if (updatedMinion.id !== req.minion.id) {
    res.status(400).send('Cannot update minion ID');
  } else {
    updatedMinion = updateInstanceInDatabase(minionsString, updatedMinion);
    res.status(200).send(updatedMinion);
  }
})

minionsRouter.delete('/:minionId', (req, res, next) => {
  deleteFromDatabasebyId(minionsString, req.minion.id);
  res.status(204).send();
});

module.exports = minionsRouter;

